<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        //쓰레드 작성 페이지 이동시 채널정보를 주입한다.
        // \View::composer('threads.create', function ($view){
        //     $view->with('channels', \App\Channel::all());
        // });

        //전체 또는 특정 페이지 이동시 채널정보를 주입한다.
        //전체 *, 특정 URI [ '1', '2' ]
        \View::composer('*', function ($view) {
            $view->with('channels', \App\Channel::all());
        });
            
        //전체 페이지에 채널정보를 주입한다.
        //\View::share('channels', \App\channel::all());
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        //dd($this->app->isLocal());
        if($this->app->isLocal()) { //env == local
        //if (env('APP_DEBUG')) {
            $this->app->register(\Barryvdh\Debugbar\ServiceProvider::class);
        //}
        }
    }
}
