<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    use Favoritable, RecordActivity;
    
    protected $guarded = [];

    protected $with = ['owner', 'favorites'];

    /**
     * 
     */
    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($reply){
            $reply->favorites->each->delete();
        });
    }
    
    /**
     * 댓글 작성자
     */
    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * 좋아요들
     */
    public function favorites()
    {
        /**
         * 다형성에 접근
         */
        return $this->morphMany(Favorite::class, 'favorited');
    }

    /**
     * 좋아요
     */
    public function favorite()
    {
        $attributes = ['user_id' => auth()->id()];
        
        if(!$this->favorites()->where($attributes)->exists()) {
            return $this->favorites()->create($attributes);
        }  
    }

    /**
     *  
     */
    public function isFavorited()
    {
        
        return !! $this->favorites->where('user_id', auth()->id())->count();
    }

    /**
     * 
     */
    public function thread()
    {
        return $this->belongsTo(Thread::class);    
    }

    /**
     * 
     */
    public function path()
    {
        return $this->thread->path(). "#reply-{$this->id}";
    }
}
