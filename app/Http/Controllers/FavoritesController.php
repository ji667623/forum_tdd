<?php

namespace App\Http\Controllers;

use App\Favorite;
use App\Reply;
use Illuminate\Http\Request;

class FavoritesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->only(['store']);
    }
    
    //
    public function store(Reply  $reply)
    {
        $reply->favorite(); 

        return back();

        //return $reply->favorite(auth()->id();
        /*
        Favorite::create([
            'user_id' => auth()->id(),
            'favorited_id'  => $reply->id,
            'favorited_type' => get_class($reply)    
        ]);
        */
    }
}
