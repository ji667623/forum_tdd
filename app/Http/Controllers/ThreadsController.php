<?php

namespace App\Http\Controllers;

use App\Thread;
use App\Channel;
use App\Filters\ThreadFilters;
use Illuminate\Http\Request;

class ThreadsController extends Controller
{
    
    //작성 부분만 권한 체크
    public function __construct()
    {
        //$this->middleware('auth')->only(['store', 'create']);
        $this->middleware('auth')->except(['index', 'show']);
    }
   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Channel $channel, ThreadFilters $filters)
    {
        // 
        $threads = $this->getThread($channel, $filters);
        
        if(request()->wantsJson()) {
            return $threads;    
        }

        return view('threads.index', compact('threads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('threads.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //dd($request->all());
        
        $this->validate($request, [
            'title' => 'required',
            'body'  => 'required',
            'channel_id' => 'required|exists:channels,id'
        ]);

        $thread = Thread::create([
            'title'     => request('title'), 
            'body'      => request('body'),
            'user_id'   => auth()->id(),
            'channel_id' => request('channel_id')
        ]);

        return redirect($thread->path())
            ->with('flash', '쓰레드 등록이 완료 되었습니다.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function show($channel, Thread $thread)
    {
        //
        //return $thread->withCount('replies')->find($thread->id);
        //dd($thread->get());
        //return $thread->load('replies.favorites')->load('replies.owner');
        return view('threads.show', [
            'thread' => $thread,
            'replies' => $thread->replies()->paginate(4)  
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function edit(Thread $thread)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Thread $thread)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function destroy($channel, Thread $thread)
    {
        //
        $this->authorize('update', $thread);
            
        // if( $thread->user_id != auth()->id() ){
             
        //     if (request()->wantsJson()){
        //         return response(['status' => 'Permission Denied'], 403);     
        //     }

        //     return redirect('/login');
            

        //     abort(403, 'Yod do not have permission to do this.');
        // }
         
        //$thread->replies()->delete();
        $thread->delete();

        if (request()->wantsJson()) {
            return response([], 204);
        } 

        return redirect('/threads');        
    }


    protected function getThread(Channel $channel, ThreadFilters $filters)
    {
        $threads = Thread::latest()->filter($filters);

        if($channel->exists) {
            $threads->where('channel_id', $channel->id);
        }

        return $threads->get();
    }
}
