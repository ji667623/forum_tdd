<?php 

namespace App\Filters;

use Illuminate\Http\Request;

abstract class Filters 
{
    protected $request, $builder;

    protected $filters = [];

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function apply($builder) 
    {
        /*
        
 
        foreach($this->getFilters() as $filter => $value){
            if (method_exists($this, $filter)) {
                $this->$filter($value);    
            }      
        }

        return $this->builder;
        */
        $this->builder = $builder;

        $this->getFilters()
            ->filter(function ($value, $filter) {
                return method_exists($this, $filter) && $value !== null;
            })
            ->each(function ($value, $filter){
                $this->$filter($value);
            });
    
        return $this->builder;

        // foreach($this->getFilters() as $filter => $value) {
        //     if(!method_exists($this, $filter) || $value === null) return;
        //     $this->$filter($value);
        // }
    }

    /**
     * 
     */
    public function getFilters()
    {
        /**
         * 배열에 사용 할수 있는 다영한 메소드를 지원하는 객체를 반환
         */
        return collect($this->request->all($this->filters));
    }
}