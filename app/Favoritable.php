<?php
namespace  App;

use Illuminate\Database\Eloquent\Model;

trait Favoritable 
{
    /**
     * 좋아요들
     */
    public function favorites()
    {
        /**
         * 다형성에 접근
         */
        return $this->morphMany(Favorite::class, 'favorited');
    }

    /**
     * 좋아요
     */
    public function favorite()
    {
        $attributes = ['user_id' => auth()->id()];

        if (!$this->favorites()->where($attributes)->exists()) {
            return $this->favorites()->create($attributes);
        }
    }
}