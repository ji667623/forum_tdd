<?php

namespace App;

use App\Filters\ThreadFilters;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{
    use RecordActivity;
    
    protected $guarded = [];

    protected $with = ['creater', 'channel'];

    protected static function boot()
    {
        parent::boot();
        /*
        */
        //$thread->withCount('replies')->find($thread->id);
        //글로벌 스코프 등록. boot 매소드는 모든 프레임워크 내 코어가 동작 된 후 호출 되며
        //프레임워크 내부 기능을 자유롭게 사용할수 있다.
        static::addGlobalScope('replyCount', function($builder) {
            $builder->withCount('replies');     
        });

        static::deleting(function($thread){
            $thread->replies->each->delete();
        });
    }

  
    /**
     * 리스트 a 태그 경로
     */
    public function path()
    {
        return "/threads/{$this->channel->slug}/{$this->id}";
    }

    /**
     * 리플
     */
    public function replies()
    {
        return $this->hasMany(Reply::class)->withCount('favorites')->latest();
    }

    /**
     * 리플 카운트
     */
    public function getReplyCountAttribute()
    {
        return $this->replies()->count();
    }

    /**
     * 작성자
     */
    public function creater()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * 리플추가
     */
    public function addReply($reply)
    {   
        //dd($this->replies());
        $this->replies()->create($reply);
    }

    /**
     * 채널관계
     */
    public function channel()
    {
        return $this->belongsTo(Channel::class);
    }

    /**
     * 검색 필터
     */ 
    
    public function scopeFilter($query, ThreadFilters  $filters)
    {
        return $filters->apply($query);
    }
    
}
