<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

/**
 *  단위 테스트 = Unit
 *  프레임 워크 전반적인 테스트 = Feature
 * 
 * 제가 질문을 정확히 파악했는지 모르겠습니다. 그럼에도 불구하고, 제 의견은 "컨트롤러는 단위 테스트하지 않는다" 입니다. 
 * HTTP 컨트롤러, 콘솔 커맨드, 큐/잡 핸들러등은 메인 영역에 머물면서, 여러가지 API들을 조합해서 흐름 제어(flow control)를 하는 최상위 함수같은 녀석이고, 딜리버리 메커니즘에 강하게 엮여 있고, *서비스/모델등 코어 레이어와도 엮여 있으므로, 완벽하게 단위 테스트하기가 불가하다고/무의미하다고 생각하기 때문입니다. 즉, 컨트롤러는 애플리케이션이 제공하는 기능(feature) 자체이므로, 의존성 격리를 통한 단위 테스트는 불필요하다 생각합니다.
 * https://wiki.modernpug.org/questions/20417576/%EB%9D%BC%EB%9D%BC%EB%B2%A8-%EC%BB%A8%ED%8A%B8%EB%A1%A4%EB%9F%AC%EC%9D%98-%EC%9C%A0%EB%8B%9B-%ED%85%8C%EC%8A%A4%ED%8A%B8-%EC%9E%91%EC%84%B1
 */

class ThreadTest extends TestCase
{
    use DatabaseMigrations;

    protected $thread;
    
    //TDD에서 construct 같은기능인가??
    public function setUp()
    {
        parent::setUp();

        $this->thread = create('App\Thread');
    }   

    /**
     * @test
     */
    public function a_user_can_browse_threads()
    {
        $this->get($this->thread->path())
            ->assertStatus(200);
    }

    /**
     * @test
     */
    public function a_user_can_see_thread_title()
    {
        $this->get($this->thread->path())
            ->assertSee($this->thread->title);
    }

    /**
     * @test
     */
    public function a_user_can_browse_threads_detail()
    {
        $this->get($this->thread->path())
            ->assertSee($this->thread->title);
    }

    /**
     * @test
     */
    public function a_user_can_read_replies_that_are_associated_with_a_thread()
    {
        //상세 페이지 갔을때 리플들을 볼수 있다.
        $reply = create('App\Reply', [
            'thread_id' => $this->thread->id  
        ]);
    
        $this->get("/threads/{$this->thread->channel->slug}/{$this->thread->id}")
            ->assertSee($reply->body);
    }


    /**
     * @test
     */
    public function a_thread_has_replies()
    {
        $this->assertInstanceOf("Illuminate\Database\Eloquent\Collection", $this->thread->replies);
    }


    /**
     * @test
     */
    public function a_thread_has_a_creater()
    {
        $this->assertInstanceOf('App\User', $this->thread->creater);
    }

    /**
     * @test
     */
    public function a_thread_can_add_a_reply()
    {
        $this->thread->addReply([
            'body'      => 'Foobar',
            'user_id'   =>  1
        ]);
 
        $this->assertCount(1, $this->thread->replies);
    }

    /**
     * @test
     */
    public function a_thread_belongs_to_a_channel()
    {
        $thread = create('App\Thread');
        $this->assertInstanceOf('App\Channel', $thread->channel); 
    }

    /**
     * @test
     */
    public function a_thread_can_make_a_string_path()
    {
        $thread = create('App\Thread');
        $this->assertEquals("/threads/{$thread->channel->slug}/{$thread->id}", $thread->path());
    }

}

