<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ReadThreadsTest extends TestCase
{
    use DatabaseMigrations;

    protected $thread;
    
    //TDD에서 construct 같은기능인가??
    public function setUp()
    {
        parent::setUp();

        $this->thread = create('App\Thread');
    }   

    /**
     * @test
     */
    public function a_user_can_read_a_single_thread()
    {
        $this->get($this->thread->path())
            ->assertSee($this->thread->title);
    }

    /**
     * @test 
     */
    public function a_user_can_read_replies_that_are_assicuated_with_a_thread()
    {
        $reply = create('App\Reply', ['thread_id' => $this->thread->id]);
        $this->get($this->thread->path())
            ->assertSee($this->thread->title);
    }

    /**
     * @test
     */
    public function a_user_can_filter_threads_according_to_a_channel()
    {
        $channel = create('App\Channel');
        $threadInChannel = create('App\Thread', ['channel_id'=> $channel->id]);
        $threadNotInChannel = create('App\Thread');
        
        $this->get("/threads/{$channel->slug}")
            ->assertSee($threadInChannel->title)
            ->assertDontSee($threadNotInChannel->title);
    }

    /**
     * @test
     */
    public function a_user_can_filter_threads_by_any_username()
    {
        $this->signIn(create('App\User', ['name' => '123']) );
        
        $threadByji6676 = create('App\Thread', [
            'user_id' => auth()->id()
        ]);
        $threadNotByji6676 = create('App\Thread');
        
        $this->get('threads?by='. auth()->user()->name)
            ->assertSee($threadByji6676->title)
            ->assertDontSee($threadNotByji6676->title);
    }   

    /**
     * @test
     */
    public function a_user_can_filter_threads_by_popularity()
    {
        //Given we have three threads
        //With 2 replies, 3 replies, and 0 replies, respcetively.
        //When I filter all threads by popularity
        //Then they should bd returned from most replies to least.
        $threadWithTwoReplies = create('App\Thread');
        create('App\Reply', ['thread_id' => $threadWithTwoReplies->id], 2);

        $threadWithThreeReplies = create('App\Thread');
        create('App\Reply', ['thread_id' => $threadWithThreeReplies->id], 3);

        $threadWithNoReplies = $this->thread;


        $response = $this->getJson('threads?popular=1')->json();

        //Then they should be returned from most replies to least.
        $this->assertEquals([3,2,0], array_column($response, 'replies_count'));  
    }
}  
