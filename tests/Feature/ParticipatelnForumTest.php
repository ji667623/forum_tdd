<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ParticipatelnForum extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function an_authenticated_user_may_participate_in_forum_threads()
    {
        //우리에게는 인증 된 사용자와 
        $this->be($user = create('App\User'));

        //기존의 스레드가 있습니다.
        $thread = create('App\Thread');

        //사용자가 스레드에 응답을 추가하면
        $reply = create('App\Reply', ['thread_id' => $thread->id]);
        $this->post("/threads/{$thread->channel->slug}/{$thread->id}/replies", $reply->toArray());
  
        //해당 응답이 페이지에 표시됩니다.  
        $this->get($thread->path()) 
            ->assertSee($reply->body);           
    }

    /**
     * @test
     */
    public function unauthenticated_user_may_not_add_replies()
    {
        $thread = create('App\Thread');

        $this->withExceptionHandling()
            ->post("/threads/{$thread->channel->slug}/{$thread->id}/replies", [])
            ->assertRedirect('/login');
    }


    /**
     * @test
     */
    public function a_reply_requires_a_body()
    {
        $thread = create('App\Thread');

        $this->publishReply(['body' => null, 'thread_id' => $thread->id, 'channel' => $thread->channel->slug])   
         ->assertSessionHasErrors('body');
    }


    public function publishReply($overrides = [])
    {
        $this->withExceptionHandling()->signIn();

        $reply = make('App\Reply', $overrides);
    
        //dd("/threads/{$overrides["channel"]}/{$overrides["thread_id"]}/replies");

        return $this->post("/threads/{$overrides["channel"]}/{$overrides["thread_id"]}/replies", $reply->toArray());
    }

    
    /**
     * @test
     */
    public function unauthenticated_users_cannot_delete_replies()
    {
        $this->withExceptionHandling();
        
        $reply = create('App\Reply');

        $this->delete("/replies/{$reply->id}")
            ->assertRedirect('login');

        $this->signIn()
            ->delete("/replies/{$reply->id}")
            ->assertStatus(403);

    }

    /**
     * @test
     */
    public function authorized_users_can_delete_replies()
    {
        $this->signIn();
        
        $reply  = create('App\Reply', ['user_id' => auth()->id()]);

        $this->delete("/replies/{$reply->id}");

        $this->assertDatabaseMissing('replies', ['id' => $reply->id]);
    }
}
