<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class Favorites extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function guestes_can_not_favorite_anything()
    {

        $reply = create('App\Reply');

        $this->withExceptionHandling()
            ->post('replies/'. $reply->id .'/favorites')
            ->assertRedirect('/login');
    }

    /**
     * @test
     */
    public function an_authenticated_user_can_favorite_any_reply()
    {
        /** 
         * /replies/id/favorites
         * /favoirtes 
         */ 
        $this->signIn();
        
        $reply = create('App\Reply');
        
        $this->post('replies/' . $reply->id . '/favorites');

        $this->assertCount(1, $reply->favorites);
    }
    

    /**
     * @test
     */
    public function an_authenticated_user_may_only_favorite_a_reply_once()
    {
        /** 
         * /replies/id/favorites
         * /favoirtes 
         */
        $this->signIn(); //로그인 

        $reply = create('App\Reply'); //리플생성
        
        try { //리플 좋아요 중복 
            $this->post('replies/' . $reply->id . '/favorites'); 
            $this->post('replies/' . $reply->id . '/favorites');
        } catch (\Exception $e) { //익셉션
            $this->fail('Did not expect to insert the same recored set twice.');
        }

        //이거 카운트 1개가 맞니?
        $this->assertCount(1, $reply->favorites);
    }
}
