<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\Activity;

class CreateThreadTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function guests_may_not_create_threads()
    {
        $this->withExceptionHandling(); 

        $this->get('/threads/create')
            ->assertRedirect('/login');
         

        //When we hit the endpoint to create a new thread
        $thread = create('App\Thread');

        //Then, when we visit the thread page.
        $this->post('/threads', $thread->toArray())
            ->assertRedirect('/login');
    }
    

    /**
     * @test
     */
    public function guests_may_not_see_create_thread_page()
    {
        $this->withExceptionHandling()->get('/threads/create')
            ->assertRedirect('/login');
    }


    /**
     * @test
     */
    public function an_authenticated_user_can_create_new_forum_threads()
    {
        //Given we heave a signed in user.
        $this->signIn();

        //When we hit the endpoint to create a new thread
        $thread = make('App\Thread');

        //Then, when we visit the thread page.
        $response = $this->post('/threads', $thread->toArray());

        //dd($thread->path());
        //dd($response->headers->get('Location'));

        //We Should see the new thread.
        $this->get($response->headers->get('Location'))
            ->assertSee($thread->title)
            ->assertSee($thread->body);
    }

    /**
     * @test
     */
    public function a_thread_requires_a_title()
    {   

        $this->publishThread(['title' => null])
            ->assertSessionHasErrors('title');
    }

    /**
     * @test
     */
    public function a_thread_requires_a_body()
    {
        $this->publishThread(['body' => null])
            ->assertSessionHasErrors('body');
    }

    /**
     * @test
     */
    public function a_thread_requires_a_channel()
    {
        factory('App\Channel', 2)->create();
        
        $this->publishThread(['channel_id' => null])
            ->assertSessionHasErrors('channel_id');

        $this->publishThread(['channel_id' => 49])
            ->assertSessionHasErrors('channel_id');
    }

    /**
     * @test
     */
    public function unauthorized_users_may_not_delete_threads()
    {
        $this->withExceptionHandling();

        $thread = create('App\Thread');

        $this->delete( $thread->path())->assertRedirect('/login');

        $this->signIn();
        $this->delete($thread->path())->assertStatus(403);
    }


    /**
     * @test
     */ 
    public function authorized_users_can_deleted_threads()
    {
        //로그인 
        $this->signIn();

        //쓰레드 생성
        $thread = create('App\Thread', ['user_id' => auth()->id() ]);
        $reply  = create('App\Reply', ['thread_id'=> $thread->id]);

        $response = $this->json('DELETE', $thread->path());

        //리스폰스 코드 확인
        $response->assertStatus(204);
        
        //삭제 되었는지 디비 확인
        $this->assertDatabaseMissing('threads', ['id' => $thread->id]);
        $this->assertDatabaseMissing('replies', ['id' => $reply->id]);
        //$this->assertDatabaseMissing('activities', ['subject_id' => $thread->id, 'subject_type' => get_class($thread)]);
        //$this->assertDatabaseMissing('activities', ['subject_id' => $reply->id, 'subject_type' => get_class($reply)]);
    
        $this->assertEquals(0, Activity::count());
    }   
   
    public function publishThread($overrides = [])
    {
        $this->withExceptionHandling()->signIn();

        $thread = make('App\Thread', $overrides);
        
        return $this->post('/threads', $thread->toArray());
    }
}
