FROM php:7.2-fpm

RUN apt-get  update && apt-get install -y libmcrypt-dev \
   mysql-client libmagickwand-dev --no-install-recommends \
    && pecl install imagick \
   && docker-php-ext-enable imagick \
&& docker-php-ext-install pdo_mysql

RUN docker-php-ext-configure opcache --enable-opcache \
    && docker-php-ext-install opcache
# Copy configuration
RUN rm -rf $PHP_INI_DIR/conf.d/docker-php-ext-opcache.ini
COPY ./opcache.ini $PHP_INI_DIR/conf.d/docker-php-ext-opcache.ini
