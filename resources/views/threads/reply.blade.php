@foreach ($replies as $reply)
    <hr/>
    <div class="row " id="reply-{{$reply->id}}">
        <div class="col"> 
            <div class="card-header bg-warning">
                <div class="level">
                    <h6 class="flex">
                        <a href="{{route('profile', $reply->owner)}}"  >
                            {{ $reply->owner->name }}
                        </a>&nbsp;said {{ $reply->created_at->diffForHumans() }}...
                    </h6>
                    
                    <div>
                        <form method="POST" action="/replies/{{$reply->id}}/favorites">
                            {{csrf_field()}}
                            <button type="submit" class="btn btn-default" {{ $reply->isFavorited() ? 'disabled' : '' }}>
                                {{$reply->favorites()->count()}}&nbsp;{{str_plural('Favorite', $reply->favorites()->count())}}
                            </button>
                        </form>
                    </div>
                </div> 
            </div>
            <div class="card">
                <div class="card-body">
                    <div>{{ $reply->body }}</div>
                </div>
                @can ('update', $reply)
                    <div class="card-footer ">
                        <form method="POST" action="/replies/{{$reply->id}}"> 
                            {{csrf_field()}}
                            {{method_field('DELETE')}}
                            <button type="submit" class="btn  btn-danger btn-xs" >Delete</button>
                        </form>
                    </div>
                @endcan
            </div>
        </div>
    </div>
@endforeach
@if($replies->count() > 0)
    <hr>
    <div class="row justify-content-center">
        {{ $replies->links() }}
    </div>
@endif
<hr/>
@if(auth()->check())
    <div class="row ">
        <div class="col "> 
            <form method="POST" action="{{ $thread->path() }}/replies" > 
                {{csrf_field()}}
                <div class="form-group">
                    <textarea name="body" id="body" class="form-control" placeholder="Have Something to Say?" rows="5"></textarea>
                </div>
                <button type="submit"  class="btn btn-default">Post</button> 
            </form>
        </div>
    </div>
    <hr>
@else 
    <p class="text-center">Please <a href="{{route('login')}}">Sign In</a> to participate in this discussion.</p>
    <hr>
@endif

