@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row ">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="level">
                        <span class="flex">
                            <a href="{{route('profile', $thread->creater)}}"  >{{ $thread->creater->name }}</a> posted :  
                            {{ $thread->title }}
                        </span>

                        @can ('update', $thread)
                            <form action="{{$thread->path()}}" method="POST"> 
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                
                                <button type="submit" class="btn btn-link">Delete Thread</button>
                            </form>
                        @endcan
                    </div>
                </div>
                        
                <div class="card-body">
                    <div>{{ $thread->body }}</div>
                </div>
            </div>
            @include ('threads.reply')
        </div>
        
        <div class="col-md-4">
             <div class="card">
                <div class="card-body">
                    This thread was published {{ $thread->created_at->diffForHumans() }} by 
                    <a href="/profiles/{{ $thread->creater->name }}" >{{ $thread->creater->name }} </a>  and currently 
                    has {{ $thread->replies_count }} <!--한개일떄 여러개일때 단수형 복수형-->{{str_plural('comment', $thread->replies_count)}}.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection