<div class="container">
    <div class="card">
        <div class="card-header">
            <div class="level">
                <span class="flex">
                    {{$heading}}
                    {{-- {{$profileUser->name}} replied to 
                    <a href="{{$activity->subject->thread->path()}}" >"{{$activity->subject->thread->title}}"</a> --}}
                </span>
            </div>
        </div>
        <div class="card-body">
            <div>
                {{$body}}
                {{-- {{ $activity->subject->body }} --}}
            </div>
        </div>
    </div>
</div>

